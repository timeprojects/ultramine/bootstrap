package org.ultramine.bootstrap.deps;

import org.apache.commons.io.IOUtils;
import org.ultramine.bootstrap.deps.IDownloadable;
import org.ultramine.bootstrap.util.HashUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;

public abstract class AbstractDownloadable implements IDownloadable
{
	protected File outputDir;
	protected File checkSumsDir;

	public void setOutputDir(File outputDir)
	{
		this.outputDir = outputDir;
	}

	public File getOutputDir()
	{
		return this.outputDir;
	}

	public void setCheckSumsDir(File checkSumsDir)
	{
		this.checkSumsDir = checkSumsDir;
	}

	protected static String copyAndDigest(InputStream in, OutputStream out) throws IOException
	{
		MessageDigest digest = HashUtil.getSHA1();
		byte[] buffer = new byte[0x10000];
		try
		{
			for(int read = in.read(buffer); read > 0; read = in.read(buffer))
			{
				digest.update(buffer, 0, read);
				out.write(buffer, 0, read);
			}
		}
		finally
		{
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(out);
		}

		return HashUtil.byteArray2Hex(digest.digest());
	}

	protected static void ensureFileWritable(File target)
	{
		if(target.getParentFile() != null && !target.getParentFile().isDirectory())
		{
			if(!target.getParentFile().mkdirs() && !target.getParentFile().isDirectory())
				throw new RuntimeException("Could not create directory " + target.getParentFile());
		}

		if(target.isFile() && !target.canWrite())
			throw new RuntimeException("Do not have write permissions for " + target + " - aborting!");
	}
}

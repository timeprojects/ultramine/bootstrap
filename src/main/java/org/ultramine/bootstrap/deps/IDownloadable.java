package org.ultramine.bootstrap.deps;

import java.io.File;
import java.io.IOException;

public interface IDownloadable
{
	void setOutputDir(File outputDir);

	File getOutputDir();

	void setCheckSumsDir(File checkSumsDir);

	void download() throws IOException;
}

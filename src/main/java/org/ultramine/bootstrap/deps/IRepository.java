package org.ultramine.bootstrap.deps;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

public interface IRepository
{
	InputStream resolve(String path) throws IOException;

	default String resolveCheckSum(String path) throws IOException
	{
		InputStream inp = resolve(path + ".sha1");
		if(inp == null)
			inp = resolve(path + ".sha");
		if(inp == null)
			return null;
		String sum = IOUtils.toString(inp).trim();
		if(sum.length() > 40)
			sum = sum.substring(0, 40);
		return sum;
	}
}

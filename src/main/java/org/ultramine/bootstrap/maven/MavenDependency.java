package org.ultramine.bootstrap.maven;

import org.ultramine.bootstrap.deps.IDependency;
import org.ultramine.bootstrap.deps.IRepository;
import org.ultramine.bootstrap.deps.IDownloadable;

import java.util.List;
import java.util.Objects;

public class MavenDependency implements IDependency
{
	public final String group;
	public final String artifactName;
	public final String version;

	public MavenDependency(String group, String artifactName, String version)
	{
		this.group = Objects.requireNonNull(group);
		this.artifactName = Objects.requireNonNull(artifactName);
		this.version = Objects.requireNonNull(version);
	}

	public MavenDependency(String name)
	{
		String[] parts = name.split(":", 3);
		this.group = parts[0];
		this.artifactName = parts[1];
		this.version = parts[2];
	}

	public String getGroup()
	{
		return group;
	}

	public String getArtifactName()
	{
		return artifactName;
	}

	public String getVersion()
	{
		return version;
	}

	public String getArtifactBaseDir()
	{
		return group.replace('.', '/') + "/" + artifactName + "/" + version;
	}

	public String getArtifactFilename(String classifier, String extension)
	{
		return artifactName + "-" + version + (classifier == null || classifier.isEmpty() ? "" : "-" + classifier) + "." + extension;
	}

	public String getArtifactPath()
	{
		return getArtifactPath(null);
	}

	public String getArtifactPath(String classifier)
	{
		return getArtifactBaseDir() + "/" + getArtifactFilename(classifier);
	}

	public String getArtifactPath(String classifier, String extension)
	{
		return getArtifactBaseDir() + "/" + getArtifactFilename(classifier, extension);
	}

	public String getArtifactFilename()
	{
		return getArtifactFilename(null);
	}

	public String getArtifactFilename(String classifier)
	{
		return getArtifactFilename(classifier, "jar");
	}

	public IDownloadable resolve(List<IRepository> repositories)
	{
		return new MavenDownloadable(repositories, this);
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o) return true;
		if(o == null || getClass() != o.getClass()) return false;
		MavenDependency that = (MavenDependency) o;
		return Objects.equals(group, that.group) &&
				Objects.equals(artifactName, that.artifactName) &&
				Objects.equals(version, that.version);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(group, artifactName, version);
	}

	@Override
	public String toString()
	{
		return group+':'+ artifactName +':'+version;
	}
}

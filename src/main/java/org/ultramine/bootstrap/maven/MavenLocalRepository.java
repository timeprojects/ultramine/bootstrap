package org.ultramine.bootstrap.maven;

import org.ultramine.bootstrap.deps.IRepository;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MavenLocalRepository implements IRepository
{
	private final File directory;

	public MavenLocalRepository(File directory)
	{
		this.directory = directory;
	}

	@Override
	public InputStream resolve(String path) throws IOException
	{
		File file = new File(directory, path.replace("/", File.separator));
		if(!file.isFile())
			return null;
		return new FileInputStream(file);
	}

	@Override
	public String toString()
	{
		return directory.getAbsolutePath();
	}
}

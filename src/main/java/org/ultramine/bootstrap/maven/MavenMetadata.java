package org.ultramine.bootstrap.maven;

import org.ultramine.bootstrap.exceptions.ApplicationErrorException;
import org.ultramine.bootstrap.versioning.DefaultArtifactVersion;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MavenMetadata
{
	private final List<DefaultArtifactVersion> versions;

	private MavenMetadata(String xmlUrl)
	{
		Document doc;
		try
		{
			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlUrl);
		}
		catch (Exception e)
		{
			throw new ApplicationErrorException(e, "error.mavenmetadata", xmlUrl);
		}
		NodeList listNodes = doc.getElementsByTagName("version");

		versions = new ArrayList<>(listNodes.getLength());
		for(int i = 0, s = listNodes.getLength(); i < s; i++)
		{
			String version = listNodes.item(i).getTextContent();
			versions.add(new DefaultArtifactVersion(version, version));
		}
		Collections.sort(versions);
	}

	public List<DefaultArtifactVersion> getVersions()
	{
		return versions;
	}

	public static MavenMetadata loadFromXML(String xmlUrl)
	{
		return new MavenMetadata(xmlUrl);
	}

	public static MavenMetadata loadFromProject(String projectUrl)
	{
		return loadFromXML(projectUrl + (projectUrl.endsWith("/") ? "" : "/") + "maven-metadata.xml");
	}

	public static MavenMetadata loadFromProject(String repoUrl, String group, String project)
	{
		return loadFromProject(repoUrl + "/" + group.replace('.', '/') + "/" + project);
	}
}
